from django.contrib import admin
from .models import Author, Book, Publisher

# Register your models here.

class AuthorAdmin(admin.ModelAdmin):
	fields = ['email', 'first_name', 'last_name']
	search_fields = ['first_name', 'last_name']


class BookAdmin(admin.ModelAdmin):
	fieldsets = [
		(None, {'fields': ['publisher', 'title', 'authors']}),
		('Date information', {
			'fields': ['publication_date'],
			'classes': ['collapse'],
		}),
	]
	list_display = ['title', 'publisher', 'was_published_recently']
	#list_filter = ['publication_date']
	search_fields = ['title']
	filter_horizontal = ['authors']


class BookInLine(admin.TabularInline):
	model = Book
	extra = 3


class PublisherAdmin(admin.ModelAdmin):
	#inline = [BookInLine]
	fields = ['website', 'name', 'address', 'city', 'state_province', 'country']
	search_fields = ['name', 'city', 'country', 'website']
	list_display = ['name', 'city', 'country', 'website']


admin.site.register(Author, AuthorAdmin)
admin.site.register(Book, BookAdmin)
admin.site.register(Publisher, PublisherAdmin)
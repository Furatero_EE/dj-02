from django.test import TestCase
from django.utils import timezone

# Create your tests here.

class BookMethodTests(TestCase):
	def test_was_published_recently_with_future_book(self):
		future_date = timezone.now().date() + datetime.timedelta(days=30)
		future_book = Book(publication_date = future_date)
		self.assertEqual(future_book.was_published_recently(), False)

class IndexViewTests(TestCase):
	def setUp(self):
		self.publisher = Publisher.objects.create(
			name='test',
			address='test',
			city='test',
			state_province='test',
			country='test',
			website='test',
		)
		self.author = Author.objects.create(
			first_name='test',
			last_name='test',
			email='test@gmail.com',
		)
		book = Book.objects.create(
			title='test',
			publisher=self.publisher,
			publication_date=timezone.now(),
		)
		self.book = book
		self.book.authors.add(self.author)

	def test_index_view_with_books(self):
		"""Books should be displayed if some books exist."""
		response = self.client.get(reverse('index'))
		self.assertEqual(response.status_code, 200)
		self.assertQuerysetEqual(
			response.context['books'],
			[repr(r) for r in Book.objects.all()],
		)

	def test_index_view_with_no_books(self):
		"""Display appropriate message if no books exist."""
		Book.objects.all().delete()
		response = self.client.get(reverse('index'))
		self.assertContains(response, "No books are available")
		self.assertQuerysetEqual(
			response.context['books'],
			[],
		)

	def test_add_author(self):
		"""Test adding authors to database"""
		self.author = Author.objects.create(
			first_name='test',
			last_name='test',
			email='test@gmail.com',
		)
		book = Book.objects.create(
			title='test',
			publisher=self.publisher,
			publication_date=timezone.now(),
		)
		self.book = book
		self.book.authors.add(self.author)
		self.assertEqual(response.status_code, 200)

	def test_search(self):
		"""Test search functionality"""
		response = self.client.get(reverse('search'))
		search = request.GET['search']
		self.assertContains(response, "")
		self.assertQuerysetEqual(
			response.context['books'],
			[repr(r) for r in Book.objects.filter(title__icontains=search)],
		)

	def test_login(self):
		"""Test login functionality"""
		user = User.objects.create(username="testuser")
		user.set_password('12345')
		user.save()

		logged_in = self.client.login(username='testuser', password='12345')
		self.assertTrue(logged_in)

	def test_logout(self):
		"""Test logout functionality"""
		# Log in
        self.client.login(username='testuser', password='12345')

        # Check response code
        response = self.client.get('/admin/')
        self.assertEquals(response.status_code, 200)

        # Check 'Log out' in response
        self.assertTrue('Log out' in response.content)

        # Log out
        self.client.logout()

    def test_detail_view(self):
    	"""Test detail view functionality"""
    	response = self.client.get(reverse('detail_view'))
		self.assertEqual(response.status_code, 200)
import datetime

from django.db import models
from django.utils import timezone
from django.dispatch import receiver
from django.core.signals import post_save

# Create your models here.

class Publisher(models.Model):
	name = models.CharField(max_length=30)
	address = models.CharField(max_length=50)
	city = models.CharField(max_length=60)
	state_province = models.CharField(max_length=30)
	country = models.CharField(max_length=50)
	website = models.URLField()

	def __str__(self):
		return self.name


class Author(models.Model):
	first_name = models.CharField(max_length=30)
	last_name = models.CharField(max_length=40)
	email = models.EmailField(' e-mail')

	def __str__(self):
		return u'%s %s' % (self.first_name, self.last_name)


class Book(models.Model):
	title = models.CharField(max_length=100)
	authors = models.ManyToManyField(Author)
	publisher = models.ForeignKey(Publisher, on_delete=models.CASCADE)
	publication_date = models.DateField()

	def __str__(self):
		return self.title

	def was_published_recently(self):
		date_today = timezone.now().date()
		return date_today - datetime.timedelta(days<=1) <= self.publication_date <= date_today

	was_published_recently.admin_order_field = 'publication_date'
	was_published_recently.boolean = True
	was_published_recently.short_description = 'Published recently?'


class Classification(models.Model):
	code = models.CharField(max_length=3)
	name = models.CharField(max_length=40)
	description = models.CharField(max_length=100)

class SavingTracker(models.Model):
	book = models.OneToOneField(Book)
	creator = models.CharField(max_length=50)
	date_created = models.CharField(max_length=20)
	date_modified = models.CharField(max_length=20)


@receiver(post_save, sender=Book)
def book_created(sender, **kwargs):
	if kwargs.get('created', False):
		SavingTracker.objects.get_or_create(book=kwargs.get('instance'), date_created=datetime.now())
	else:
		SavingTracker.objects.get_or_create(book=kwargs.get('instance'), date_modified=datetime.now())
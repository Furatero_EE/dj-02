from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.views.generic import ListView, View
from django.core import mail
from books.models import Book, Publisher, Author
from .forms import ContactForm

import logging

# Create your views here.

#def search_form(request):
	#return render(request, 'search_form.html')

logger = logging.getLogger(__name__)

class SearchBaseView(View):
	template_name = ''
	base_view = 'search_form.html'
	model = None

	def get(self, request, *args, **kwargs):
		q = self.kwargs.get('q', '')
		errors = []

		if not q:
			errors.append('Enter a search term.')
		elif len(q) > 20:
			errors.append('Please enter at most 20 characters.')
		else:
			object_list = self.model.objects.filter(title__icontains=q)
			request.session['results'] = object_list
			return render(request, self.template_name, {'object_list': object_list, 'query': q})
		return render(request, self.base_view, {'errors': errors})


class BookSearchView(SearchBaseView):
	template_name = 'search_results.html'
	model = Book


class AuthorSearchView(SearchBaseView):
	template_name = 'search_author_results.html'
	model = Author

class PublisherSearchView(SearchBaseView):
	template_name = 'search_publisher_results.html'
	model = Publisher


def search(request):
	errors = []
	if 'q' in request.GET:
		q = request.GET['q']
		if not q:
			errors.append('Enter a search term.')
		elif len(q) > 20:
			errors.append('Please enter at most 20 characters.')
		else:
			books = Book.objects.filter(title__icontains=q)
			return render(
				request,
				'search_results.html',
				{'books': books, 'query': q},
			)
	return render(request, 'search_form.html', {'errors': errors})


def search_publisher(request):
	errors = []
	if 'q' in request.GET:
		q = request.GET['q']
		if not q:
			errors.append('Enter a search term.')
		elif len(q) > 20:
			errors.append('Please enter at most 20 characters.')
		else:
			publishers = Publisher.objects.filter(name__icontains=q)
			return render(
				request,
				'search_publisher_results.html',
				{'publishers': publishers, 'query': q}
			)
	return render(request, 'search_form.html', {'errors': errors})


def search_author(request):
	errors = []
	if 'q' in request.GET:
		q = request.GET['q']
		if not q:
			errors.append('Enter a search term.')
		elif len(q) > 20:
			errors.append('Please enter at most 20 characters.')
		else:
			authors = Author.objects.filter(first_name__icontains=q)
			return render(
				request,
				'search_author_results.html',
				{'authors': authors, 'query': q}
			)
	return render(request, 'search_form.html', {'errors': errors})


def contact(request):
	if request.method == 'POST': # If the form has been submitted...
		form = ContactForm(request.POST) # A form bound to the POST data
		if form.is_valid(): # All validation rules pass
			# Process the data in form.cleaned_data
			# ...
			return HttpResponseRedirect('/thanks/')
		else:
			form = ContactForm() # An unbound form
		return render(
			request,
			'contact.html',
			{'form': form},
		)


def index(request):
	books = Book.objects.all()
	return render(request, 'books/books.html', {'books': books})


def detail(request, pk):
	try:
		book = Book.objects.get(pk=pk)
	except Book.DoesNotExist:
		raise Http404()

	return render(request, 'books/book_detail.html', {'book': book})


def contact_us(request):
	errors = []
	if 'fullname' in request.GET and 'subject' in request.GET and 'message' in request.GET:
		fullname = request.GET['fullname']
		subject = request.GET['subject']
		message = request.GET['message']
		if not fullname and not subject and not message:
			errors.append('Fill up all fields.')
		#elif len(q) > 20:
		#	errors.append('Please enter at most 20 characters.')
		else:
			connection = mail.get_connection()
			connection.open()

			email = EmailMessage(
				subject,
				message,
				'%s@example.com' % fullname,
				['sample@yopmail.com'],
				['samplecc@yopmail.com'],
				connection=connection,
			)
			email.send()
			connection.close()
			logger.debug('Sent email')
	return render(request, 'contact_us.html', {'errors': errors})
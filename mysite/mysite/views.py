import datetime

from django.http import HttpResponse, Http404
from django.template.loader import get_template
from django.shortcuts import render
from django.views.generic import TemplateView, DetailView, View


# Create your views here.

class HelloView(View):
	template_name = 'hello.html'

	def get(self, request):
		return render(
			request,
			self.template_name,
			{'hello': "This is me. Hello World!"},
		)


class CurrentDateTimeView(View):
	template_name = 'current_datetime.html'
	now = datetime.datetime.now()

	def get(self, request):
		return render(
			request,
			self.template_name,
			{'current_date': now},
		)


class HoursAheadView(View):
	template_name = 'hours_ahead.html'

	def get(self, request, offset):
		dt = datetime.datetime.now() + datetime.timedelta(hours = offset)

		try:
			offset = int(offset)
		except ValueError:
			raise Http404()

		return render(
			request,
			self.template_name,
			{'hour_offset': offset, 'next_time': dt},
		)


class MathComputeView(View):
	template_name = 'math_compute.html'

	def get(self, request, *args, **kwargs):
		number1 = kwargs.get('number1')
		number2 = kwargs.get('number2')
		number3 = kwargs.get('number3')
		numsum = 0
		numdiff = 0
		numprod = 0
		numquo = 0

		if number3 is None:
			number3 = 0
			numprod = number1 * number2
			numquo = number1 / number2
		else:
			numprod = number1 * number2 * number3
			numquo = number1 / number2/ number3


		numsum = number1 + number2 + number3
		numdiff = number1 - number2 - number3
		return render(
			request,
			self.template_name,
			{'numsum': numsum, 'numdiff': numdiff,
			 'numprod': numprod, 'numquo': numquo,
			 'number1': number1, 'number2': number2,
			 'number3': number3},
		)


class ValidDateView(View):
	template_name = 'valid_date.html'

	def get(self, request, year, month, day):
		yearstr = str(year)
		monthstr = str(month)
		daystr = str(day)
		inputdate = yearstr+"/"+monthstr+"/"+daystr
		valid = ''

		try:
			datetime.datetime.strptime(inputdate, "%Y/%m/%d")
			valid = 'valid'
			return render(
				request,
				self.template_name,
				{'year': year, 'month': month, 'day': day,
				 'is_valid': valid},
			)
		except ValueError as error:
			valid = 'invalid'
			return render(
				request,
				self.template_name,
				{'year': year, 'month': month, 'day': day,
				 'is_valid': valid},
			)


class SearchHistoryView(View):
	template_name = ''

	def get(self, request):
		return render(
			request,
			self.template_name,
			{'stored': request.session['results']},
		)



def hello(request):
	#return HttpResponse("Hello world")
	return render(
		request,
		'hello.html',
		{'hello': "This is me. Hello World!"},
	)


def current_datetime(request):
	now = datetime.datetime.now()
	#html = "<html><body>It is now %s.</body></html>" % now
	#template = get_template('current_datetime.html')
	#html = template.render({'current_date': now})
	#return HttpResponse(html)
	return render(
		request,
		'current_datetime.html',
		{'current_date': now},
	)


def hours_ahead(request, offset):
	try:
		offset = int(offset)
	except ValueError:
		raise Http404()

	dt = datetime.datetime.now() + datetime.timedelta(hours = offset)
	#html = "<html><body>In %s hour(s), it will be %s.</body></html>" % (offset, dt)
	#return HttpResponse(html)
	return render(
		request,
		'hours_ahead.html',
		{'hour_offset': offset, 'next_time': dt},
	)


def math_compute(request, *args, **kwargs):
	number1 = kwargs.get('number1')
	number2 = kwargs.get('number2')
	number3 = kwargs.get('number3')
	numsum = 0
	numdiff = 0
	numprod = 0
	numquo = 0

	if number3 is None:
		number3 = 0
		numprod = number1 * number2
		numquo = number1 / number2
	else:
		numprod = number1 * number2 * number3
		numquo = number1 / number2/ number3


	numsum = number1 + number2 + number3
	numdiff = number1 - number2 - number3

	#html = "<html><body>Sum is: %d<br>Difference is: %d<br>Product is: %d<br>Quotient is: %.2f</body></html>" % (numsum, numdiff, numprod, numquo)
	#return HttpResponse(html)
	return render(
		request,
		'math_compute.html',
		{'numsum': numsum, 'numdiff': numdiff,
		 'numprod': numprod, 'numquo': numquo,
		 'number1': number1, 'number2': number2,
		 'number3': number3},
	)


def valid_date(request, year, month, day):
	yearstr = str(year)
	monthstr = str(month)
	daystr = str(day)
	inputdate = yearstr+"/"+monthstr+"/"+daystr
	valid = ''

	try:
		datetime.datetime.strptime(inputdate, "%Y/%m/%d")
		#html = "<html><body>Valid date</body></html>"
		#return HttpResponse(html)
		valid = 'valid'
		return render(
			request,
			'valid_date.html',
			{'year': year, 'month': month, 'day': day,
			 'is_valid': valid},
		)
	except ValueError as error:
		#html = "<html><body>Invalid date or format</body></html>"
		#return HttpResponse(html)
		valid = 'invalid'
		return render(
			request,
			'valid_date.html',
			{'year': year, 'month': month, 'day': day,
			 'is_valid': valid},
		)


def current_url_view_good(request):
	return HttpResponse("Welcome to the page at %s" % request.path)


def ua_display_good1(request):
	try:
		ua = request.META['HTTP_USER_AGENT']
	except KeyError:
		ua = 'unknown'
	return HttpResponse("Your browser is %s" % ua)


def display_meta(request):
	values = request.META.items()
	sorted(values)
	html = []

	for k, v in values:
		html.append('<tr><td>%s</td><td>%s</td></tr>' % (k, v))

	return HttpResponse('<table>%s</table>' % '\n'.join(html))


def display_something(request):
	errors = []
	if 'q' in request.GET:
		q = request.GET['q']
		if not q:
			errors.append('Enter a search term.')
		elif len(q) > 20:
			errors.append('Please enter at most 20 characters.')
		else:
			return render(
				request,
				'display_meta.html',
				{'something': q}
			)
	return render(request, 'input_meta.html', {'errors': errors})
"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView

from mysite.views import hello, current_datetime, hours_ahead, math_compute, current_url_view_good, valid_date, ua_display_good1, display_meta, display_something
from mysite.views import HelloView, CurrentDateTimeView, HoursAheadView, MathComputeView, ValidDateView, SearchHistoryView

urlpatterns = [
    #path('', HelloView.as_view()),
    path('', CurrentDateTimeView),
    path('<int:offset>', HoursAheadView),
    path('math/<int:number1>/<int:number2>', MathComputeView),
    path('valid-date/<int:year>/<int:month>/<int:day>', ValidDateView),
    path('search_history/', SearchHistoryView),
    path('books/', include('books.urls')),
    path('admin/', admin.site.urls),
]
